
## Dataset

The dataset used for this project is concerned with the **Melbourne Housing Market** for the last six months. A **Multiple Linear Regression** model is used to predict the house prices in Melbourne using some of the following variables:

- `Price`: Price in dollars (numeric) - **response**
- `Suburb`: Self explanitory (factor)
- `Type`: br - bedroom(s); h - house,cottage,villa, semi,terrace; u - unit, duplex; t - townhouse; dev site - development site; o res - other residential (factor)
- `Regionname`: General Region (West, North West, North, North east ...etc) (factor)
- `Rooms`: Number of rooms (numeric)
- `Distance`: Distance from Central Business District (numeric)
- `Bathroom`: Number of bathrooms (numeric)
- `Car`: Number of carspots (numeric)
- `Landsize`: Self explanitory (numeric)
- `YearBuilt`: Year the house was built (numeric)
- `Propertycount`: Number of properties that exist in the suburb (numeric)

The dataset ("`Melbourne_housing_extra_data.csv`") is available on [Kaggle](https://www.kaggle.com/). It can be downloaded by going to [Melbourne Housing Market](https://www.kaggle.com/anthonypino/melbourne-housing-market) page.